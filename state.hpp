#ifndef STATE_HPP
#define STATE_HPP

#include "event.hpp"
#include "additionaldata.hpp"

#include <memory>
#include <map>

class State
{
public:
	enum STATES
	{
	 STATE_OFF = 0,
	 STATE_ON,
	 STATE_STOPPED,
	 STATE_PLAYING,
	 STATES_EXTENSION
	};

	typedef int StateType;
	
	State(StateType id, std::shared_ptr<State> parent=nullptr);
	//nie zakładam dziedziczenia, więc nie definiuję wirtualnego destruktora, żeby zaoszczędzić na vtable. W najgorszym przypadku można użyć data
	StateType GetStateId();
	AdditionalData* GetData();
	void SetData(const AdditionalData& newData);
	std::weak_ptr<State> GetParent();
	std::weak_ptr<State> SetParent(std::weak_ptr<State> newParent);

	std::weak_ptr<State> GetStartState();
	std::weak_ptr<State> SetStartState(std::weak_ptr<State> state);

	void RegisterEvent(Event::EventType event, std::weak_ptr<State>);
	std::tuple<bool, std::weak_ptr<State>> HandleEvent(const Event& event);
	const std::map<Event::EventType, std::weak_ptr<State>>& GetEvents();

private:
	//int, żeby można było łatwo używać rozszeżonych statusów
	StateType stateId;
	std::unique_ptr<AdditionalData> data;
	//chcę, żeby za tworzenie i usuwanie stanów była odpowiedzialna klasa im "nadrzędna", więc nie byłoby dobrze, gdyby stan wciąż istniał w grafie po usunięciu
	std::weak_ptr<State> parent, startState;
	std::map<Event::EventType, std::weak_ptr<State>> eventMap;
};
	
#endif
