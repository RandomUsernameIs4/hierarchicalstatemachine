#include "../statemachine.hpp"

#include <iostream>

enum EXTENSION
	{
	 STATE_INFO = State::STATES_EXTENSION
	};

int main(int argc, char* argv[])
{
	StateMachine sm;
	sm.AddState(State::STATE_OFF);
	sm.AddState(STATE_INFO);
	if(sm.GetRegisteredStates().size() == 2)
		return 0;
	return 1;
}
