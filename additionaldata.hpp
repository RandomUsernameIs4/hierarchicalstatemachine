#ifndef ADDITIONAL_DATA_HPP
#define ADDITIONAL_DATA_HPP

#include <memory>

class AdditionalData
{
public:
	virtual std::unique_ptr<AdditionalData> Clone() const;
	virtual ~AdditionalData() = default;
	AdditionalData(const AdditionalData&) = delete;
  AdditionalData& operator=(const AdditionalData&) = delete;
	AdditionalData(AdditionalData&&) = delete;
  AdditionalData& operator=(AdditionalData&&) = delete;
};

#endif
