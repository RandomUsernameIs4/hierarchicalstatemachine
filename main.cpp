#include "statemachine.hpp"

#include <iostream>

int main(int argc, char* argv[])
{
	StateMachine sm;
	sm.AddState(State::STATE_OFF);
	sm.AddState(State::STATE_STOPPED, State::STATE_ON);
	sm.AddState(State::STATE_PLAYING, State::STATE_ON);
	sm.AddTransition(State::STATE_OFF, Event::EVENT_POWER_ON, State::STATE_ON);
	sm.AddTransition(State::STATE_STOPPED, Event::EVENT_START, State::STATE_PLAYING);
	sm.AddTransition(State::STATE_PLAYING, Event::EVENT_STOP, State::STATE_STOPPED);

	for(auto eventId: {Event::EVENT_POWER_ON, Event::EVENT_START})
	{
		Event event(eventId);
		sm.HandleEvent(event);
	}
	std::cout<<"Current state is: "<<sm.GetCurrentState()<<std::endl;
	return 0;
}
