#ifndef STATE_MACHINE_HPP
#define STATE_MACHINE_HPP

#include "state.hpp"

#include <memory>
#include <vector>
#include <list>

class StateMachine
{
public:
	std::shared_ptr<State> AddState(State::StateType state, State::StateType parent);
	std::shared_ptr<State> AddState(State::StateType state, std::shared_ptr<State> parent=nullptr);
	std::vector<State::StateType> GetRegisteredStates();
	std::shared_ptr<State> AddTransition(State::StateType sourceState, Event::EventType event, State::StateType targetState);
	std::list<std::tuple<State::StateType, Event::EventType, State::StateType>> GetTransitions();
	std::tuple<bool, std::shared_ptr<State>> HandleEvent(Event event);
	State::StateType GetCurrentState();
protected:
	std::map<State::StateType, std::shared_ptr<State>> states;
	std::shared_ptr<State> currentState;
};

#endif
