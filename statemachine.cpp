#include "statemachine.hpp"

#include <iostream>

std::shared_ptr<State> StateMachine::AddState(State::StateType state, State::StateType parent)
{
	std::shared_ptr<State> parentState;
	if(states.count(parent))
		parentState = states[parent];
	else
		parentState = AddState(parent);
	auto newState = AddState(state, parentState);
	if(parentState->GetStartState().expired())
		parentState->SetStartState(newState);
	return newState;
}

std::shared_ptr<State> StateMachine::AddState(State::StateType state, std::shared_ptr<State> parent/*=nullptr*/)
{
	//@TEST
	if(states.count(state))
		std::cerr<<"StateMachine: AddState: state "<<state<<" already registered!"<<std::endl;
	auto ret = states[state] = std::make_shared<State>(state, parent);
	if(!currentState)
		currentState = ret;
	return ret;
}

std::vector<int> StateMachine::GetRegisteredStates()
{
	std::vector<int> ret(states.size());
	int i = 0;
	for(auto& [stateType, state]: states)
		ret[i++] = stateType;
	return ret;
}

std::shared_ptr<State> StateMachine::AddTransition(State::StateType sourceState, Event::EventType event, State::StateType targetState)
{
	std::shared_ptr<State> SourceState, TargetState;
	for(auto& [state, ptr]: {std::make_tuple(sourceState, std::ref(SourceState)),
		                         std::make_tuple(targetState, std::ref(TargetState))})
	{
		if(!states.count(state))
			ptr = AddState(state);
		else
			ptr = states[state];
	}
	SourceState->RegisterEvent(event, TargetState);
	return SourceState;
}

std::list<std::tuple<State::StateType, Event::EventType, State::StateType>> StateMachine::GetTransitions()
{
	std::list<std::tuple<State::StateType, Event::EventType, State::StateType>> ret;
	for(auto& [stateType, state]: states)
		for(const auto& [eventType, targetState]: state->GetEvents())
			if(!targetState.expired())
				ret.push_back(std::make_tuple(stateType, eventType, targetState.lock()->GetStateId()));
	return ret;
}

std::tuple<bool, std::shared_ptr<State>> StateMachine::HandleEvent(Event event)
{
	if(!currentState)
		return std::make_tuple(false, std::shared_ptr<State>());

	auto [handled, newState] = currentState->HandleEvent(event);
	if(handled)
	{
		currentState = newState.lock(); 
		while(!currentState->GetStartState().expired())
			currentState = currentState->GetStartState().lock();	
	}
	
	return std::make_tuple(handled, currentState);
}

State::StateType StateMachine::GetCurrentState()
{
	return currentState->GetStateId();
}
