#include "event.hpp"

Event::Event(Event::EventType type):
	type(type)
{
}

Event::~Event()
{
}

Event::Event(const Event& other)
{
	type = other.type;
	if(other.data)
		data = other.data->Clone();
}

Event& Event::operator=(const Event& other)
{
	type = other.type;
	data.reset();
	if(other.data)
		data = other.data->Clone();
}

Event::Event(Event&& other)
{
	type = other.type;
	data.swap(other.data);
}

Event& Event::operator=(Event&& other)
{
	type = other.type;
	data.reset();
	data.swap(other.data);
}

Event::EventType Event::GetType() const
{
	return type;
}

AdditionalData* Event::GetData()
{
	return data.get();
}

void Event::SetData(const AdditionalData& newData)
{
	data = newData.Clone();
}
