#include "state.hpp"

#include <iostream>

State::State(State::StateType id, std::shared_ptr<State> parent/*=nullptr*/):
	stateId(id),
	parent(parent),
	data(nullptr)
{
}

State::StateType State::GetStateId()
{
	return stateId;
}

AdditionalData* State::GetData()
{
	return data.get();
}

void State::SetData(const AdditionalData& newData)
{
	data = newData.Clone();
}

std::weak_ptr<State> State::GetParent()
{
	return parent;
}

std::weak_ptr<State> State::SetParent(std::weak_ptr<State> newParent)
{
	parent = newParent;
	return parent;
}

std::weak_ptr<State> State::GetStartState()
{
	return startState;
}
	
std::weak_ptr<State> State::SetStartState(std::weak_ptr<State> state)
{
	startState = state;
	return startState;
}

void State::RegisterEvent(Event::EventType event, std::weak_ptr<State> state)
{
	if(eventMap.count(event))
		std::cerr<<"State: RegisterEvent: event "<<event<<" already registered!"<<std::endl;
	eventMap[event] = state;
}

std::tuple<bool, std::weak_ptr<State>> State::HandleEvent(const Event& event)
{
	auto transitionIt = eventMap.find(event.GetType());
	if(transitionIt != eventMap.end())
		return std::make_tuple(true, transitionIt->second);
	else if (!parent.expired())
		return parent.lock()->HandleEvent(event);
		
	return std::make_tuple(false, std::weak_ptr<State>());
}

const std::map<Event::EventType, std::weak_ptr<State>>& State::GetEvents()
{
	return eventMap;
}
