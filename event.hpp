#ifndef EVENT_HPP
#define EVENT_HPP

#include "additionaldata.hpp"

class Event
{
public:
	enum EVENT_TYPES
		{
		 EVENT_POWER_ON=0,
		 EVENT_POWER_OFF,
		 EVENT_START,
		 EVENT_STOP,
		 EVENT_TYPES_EXTENSION
		};

	typedef int EventType;

	Event(EventType type);
	~Event();
	Event(const Event& other);
	Event& operator=(const Event& other);
	Event(Event&& other);
	Event& operator=(Event&& other);

	EventType GetType() const; 
	
	AdditionalData* GetData();
	void SetData(const AdditionalData& data);
	
protected:
	EventType type;
	std::unique_ptr<AdditionalData> data;
};

#endif
